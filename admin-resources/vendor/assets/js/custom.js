$(document).ready(function(){

  // Add and Edit Industry
  $(".editBtn").click(function(){
      var name = $(this).closest('tr').find('.industryName').text();
      var id = $(this).closest('tr').find('.industryId').text();
      $("#industryTitle").text('Edit Industry');
      $("input[name='new_ind']").val(name);
      $("input[name='industryId']").val(id);
      $("#industryBtn").attr('name', 'edit_industry');
  });


  // Close success alerts
  $(".close").click(function(){
    $(this).parent().remove();
  });


  // Add and Edit Interest
  $(".editInterestBtn").click(function(){
      var name = $(this).closest('tr').find('.interestName').text();
      var id = $(this).closest('tr').find('.interestId').text();
      $("#interestTitle").text('Edit Interest');
      $("input[name='new_int']").val(name);
      $("input[name='interestId']").val(id);
      $("#interestBtn").attr('name', 'edit_interest');
  });

});
