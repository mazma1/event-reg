<?php
  $id = $_GET['event_id'];
?>
  <!-- Connect to the database and retrieve required data -->
  <?php
      // Connect to the database
      require("../../includes/db-connectvars.php");

      // Retrieve the list of INDUSTRIES from the database for Industries options
      $industry_sql = "SELECT * FROM industry ORDER BY industry ASC";
      $industry_result = mysqli_query($conn, $industry_sql);

      // Store EACH row of the industry table ($industry_result) in a variable
      //$industry_row = mysqli_fetch_array($industry_result);


      // Retrieve the list of INTERESTS from the database for interests option
      $interest_sql = "SELECT * FROM interest";
      $interest_result = mysqli_query($conn, $interest_sql);

      // Store EACH row of the interest table ($interest_result) in a variable
      //$interest_row = mysqli_fetch_array($interest_result);

      // Retrieve a PARTICULAR event with the given ID
      $event_sql = "SELECT * FROM events WHERE id = $id" ;
      $event_result = mysqli_query($conn, $event_sql);


      // --SUBMITTING USER DATA TO DATABASE--
      // define variables and set to empty values
      $first_name = $last_name = $email = $mobile_number = $industry = $interest = "";
      $first_nameErr = $last_nameErr = $emailErr = $mobile_numberErr = $industryErr = $interestErr = "";

      //if form has been submitted, process the form contents...
      if (isset($_POST['submit'])) {
        //Collect Data submitted by user
        $first_name = $_POST["firstname"];
        $last_name = $_POST["lastname"];
        $email = $_POST["email"];
        $mobile_number = $_POST["mobile"];
        $industry = $_POST["industry"];
        $interest = $_POST["interest"];
        $output_form = false; //FLAG

        // Further form validation
        function test_input($data) {
          $data = trim($data);
          $data = stripslashes($data);
          $data = htmlspecialchars($data);
          return $data;
        }

        // --CHECK IF USER DATA IS ERROR FREE--
        // First Name
        if (empty($first_name)) {
          $first_nameErr = "* First Name is required";
        }
        else {
          $first_name = test_input($first_name);
          // check if name only contains letters and whitespace
          if (!preg_match("/^[a-zA-Z ]*$/", $first_name)) {
            $first_nameErr = "Only letters and white space allowed";
          }
        }
        $output_form = true;

        // Last Name
        if (empty($last_name)) {
          $last_nameErr = "* Last Name is required";
        }
        else {
          $last_name = test_input($last_name);
          // check if name only contains letters and whitespace
          if (!preg_match("/^[a-zA-Z ]*$/", $last_name)) {
            $last_nameErr = "Only letters and white space allowed";
          }
        }
        $output_form = true;

        // Email
        if (empty($email)) {
          $emailErr = "* Email is required";
        }
        else {
          $email = test_input($email);
          // check if e-mail address is well-formed
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailErr = "Invalid email format";
          }
        }
        $output_form = true;

        // Mobile Number
        if (empty($mobile_number)) {
          $mobile_numberErr = "* Please enter your mobile number";
        }
        else {
          $mobile_number = test_input($mobile_number);
          // check if mobile number contains only numbers
          if ( !is_numeric($mobile_number) ) {
            $mobile_numberErr = "Please enter a DIGITS only valid mobile number";
          }
        }
        $output_form = true;

        //Industry
        if (empty($industry)) {
          $industryErr = "* Please select your Industry of Operation";
        }
        $output_form = true;

        //Interest
        if (empty($interest)) {
          $interestErr = "* Please select your area of interest";
        }
        $output_form = true;

        if ( empty($first_nameErr) && empty($last_nameErr) && empty($emailErr) && empty($mobile_numbereErr) && empty($industryErr) && empty($interestErr) ) {

          //Check if User has registered for any event before
          $existing_user_sql = "SELECT * FROM users WHERE email = '$email'";
          $existing_user_result = mysqli_query($conn, $existing_user_sql);

          //If user is not existing..
          if (mysqli_num_rows($existing_user_result) == 0) {

            //Check if email has registered for that particular event
            $emailcheck_sql = "SELECT users.email, registrants.events_id
                               FROM users
                               INNER JOIN registrants
                               ON users.id = registrants.users_id
                               WHERE email = '$email' AND events_id = '$id' ";
            $emailcheck_data = mysqli_query($conn, $emailcheck_sql);

            if (mysqli_num_rows($emailcheck_data) == 0) {

              // Insert the user data into user table
              $submit_sql = mysqli_query($conn, "INSERT INTO users (first_name, last_name, mobile_number, email, industry, interest) VALUES ('$first_name', '$last_name', '$mobile_number', '$email', '$industry', '$interest')");

              // Record the event the user registered for in the registrants table
              $get_user_id = mysqli_query($conn, "SELECT Max(id) as userid FROM users");

              while($row = mysqli_fetch_array($get_user_id)) {
                $registrant_sql = "INSERT INTO registrants (users_id, events_id) VALUES ('$row[userid]', '$id')";
                mysqli_query($conn, $registrant_sql);
              }

              // Confirm success with the user
              // Redirect to the success page
              //$success_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '?page=success';
              //header('Location: ' . $success_url);
              $_SESSION['successful'] = 'Your registration was successful!';
              $first_name = $last_name = $email = $mobile_number = $industry = $interest = "";
            }
            else {
              // This email already exists, so display an error message
              $emailErr = '<span class="error text-right">* This email address is already registered for this event. Please use a different address.</span>';
              $email = " ";
            }
          }

          // If user already exists, retrieve the user ID
          else {

            //Check if email has registered for that particular event
            $existing_sql = "SELECT users.email, registrants.events_id
                               FROM users
                               INNER JOIN registrants
                               ON users.id = registrants.users_id
                               WHERE email = '$email' AND events_id = '$id' ";
            $existing_data = mysqli_query($conn, $existing_sql);

            if (mysqli_num_rows($existing_data) == 0) {

              // Record the event the user registered for in the registrants table
              $id_sql = "SELECT * FROM users WHERE email = '$email'";
              $id_result = mysqli_query($conn, $id_sql);

              while ($id_row = mysqli_fetch_array($id_result)) {
                $register_sql = "INSERT INTO registrants (users_id, events_id) VALUES ('$id_row[id]', '$id')";
                mysqli_query($conn, $register_sql);
              }

              $_SESSION['successful'] = 'Your registration was successful!';
              $first_name = $last_name = $email = $mobile_number = $industry = $interest = "";
            }

            else {
              // This email already exists, so display an error message
              $emailErr = '<span class="error text-right">* This email address is already registered for this event. Please use a different address.</span>';
              $email = " ";
            }
          }

        }
      }

      //...else if the form has not been submitted, display the form
      else {
        $output_form = true;
      }

  ?>
  <div id="wrapper">

      <!-- Main -->
      <div id="main">
        <div class="inner">

          <!-- Start: Banner Display -->
          <?php while ($event_row = mysqli_fetch_array($event_result)) {

            $banner = $event_row['banner'];

            if ($banner != null) { ?>
              <span class="image fit pt24">
                <img src="../admin/pages/upload/<?=$banner?>" alt="<?php echo $event_row['event']; ?>" style="height: 350px; " />
              </span>
            <?php } else { ?>
              <span class="image fit pt24">
                <img src="../../user-resources/vendor/images/pic11.jpg" alt="" class="mt24">
              </span>
            <?php } ?>
          <!-- End: Banner Display -->

          <header class="pt16" id="header" >
            <h3 class="title-font">Registration Form</h3>
          </header>

          <!-- Start: 2-Column GRID -->
          <div class="container-fluid">

            <!-- Start: Display successful registration msg-->
            <?php if(isset($_SESSION['successful'])) : ?>
              <div class="alert alert-success" style="margin: 0 16px 20px 16px;">
                  <!--<button type="button" aria-hidden="true" class="close">×</button>-->
                  <span><?php echo $_SESSION['successful']; ?></span>
              </div>
              <?php unset($_SESSION['successful']);?>
            <?php endif;  ?>
            <!-- End: Display successful registration msg -->

            <div class="row">
              <?php if ($output_form) { ?>

                <!-- Start: Form Section -->
                <div class="col-sm-12 col-md-7">
                  <form method="post" action="?page=form&event_id=<?php echo $id; ?>" class="mt40">
                    <div >
                      <div class="12u$">
                        <label class="form">First Name</label><span class="error"><?php echo $first_nameErr; ?></span>
                        <input type="text" name="firstname" value="<?php echo $first_name;?>" placeholder="First Name" class="mb16"><br>
                      </div>

                      <div class="12u$">
                        <label class="form">Last Name</label><span class="error"><?php echo $last_nameErr; ?></span>
                        <input type="text" name="lastname" value="<?php echo $last_name;?>" placeholder="Last Name" class="mb32">
                      </div>

                      <div class="12u$">
                        <label class="form">Email</label><span class="error"><?php echo $emailErr; ?></span>
                        <input type="email" name="email" value="<?php echo $email;?>" placeholder="Email" class="mb32">
                      </div>

                      <div class="12u$">
                        <label class="form">Mobile Number</label><span class="error"><?php echo $mobile_numberErr; ?></span>
                        <input type="text" maxlength="11" minlength="11" name="mobile" value="<?php echo $mobile_number;?>" placeholder="Mobile Number" class="mb32">
                      </div>
                      <!-- Break -->
                      <div class="12u$">
                        <label class="form">Industry</label><span class="error"><?php echo $industryErr; ?></span>
                        <div class="select-wrapper">
                          <select name="industry" class="mb32">
                            <option value="<?php echo $industry;?>">- Category
                               -</option>

                              <?php
                                //Add each row of the industry table to the options
                                while ($industry_row = mysqli_fetch_array($industry_result)) {
                                  echo '<option value = ' . $industry_row['id'];
                                  if ($industry == $industry_row['id']) {
                                    echo " selected";
                                  }
                                  echo '>' . $industry_row['industry'] . '</option>';
                                }
                              ?>
                          </select>
                        </div>
                      </div>

                      <div class="12u$">
                        <label class="form">Interest</label><span class="error"><?php echo $interestErr; ?></span>
                        <div class="select-wrapper">
                          <select name="interest" class="mb32">
                            <option value="<?php echo $interest;?>">- Option
                               -</option>

                            <?php
                              //Add each row of the industry table to the options
                              while ($interest_row = mysqli_fetch_array($interest_result)) {
                                echo '<option value = ' . $interest_row['id'];
                                if ($interest == $interest_row['id']) {
                                  echo " selected";
                                }
                                echo '>' . $interest_row['interest'] . '</option>';
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <!-- Break -->


                      <div class="12u$">
                        <input type="submit" name="submit" value="Register" class="special mt24">
                        <!--<input type="submit" name="event-id" value="<?php echo $id;?>" class="special mt24" style="display:none;">-->
                      </div>
                    </div>
                  </form>
                </div>
                <!-- End: Form Section -->

              <?php } ?>

              <!-- Start: Course Content Section -->
              <div class="col-sm-12 col-md-4 col-md-offset-1 mt32">
                <div class="box">
                  <!-- Event Title -->
                  <div class="mb32">
                    <h3 class="font mt8">Event Title</h3>
                    <?php
                      //while ($event_row = mysqli_fetch_array($event_result)) {
                        echo "<span class='font'>" . $event_row['event'] . "</span>";
                      //}
                    ?>
                  </div>

                  <!-- Event Description -->
                  <div class="mb32">
                    <h3 class="font">Event Description</h3>
                    <?php
                      //while ($event_row = mysqli_fetch_array($event_result)) {
                        echo "<span class='font'>" . $event_row['event_desc'] . "</span>";
                      //}
                    ?>
                  </div>

                  <!-- Event Cost -->
                  <div class="mb32">
                    <h3 class="font">Cost</h3>
                    <?php
                      //while ($event_row = mysqli_fetch_array($event_result)) {
                        if ($event_row['cost'] == 0) {
                          echo "<span class='font'>Event is FREE!</span>";
                        }
                        else {
                            echo "<span class='font'>&#8358;" . $event_row['cost'] . "</span>";
                        }
                      //}
                    }?>
                  </div>
                </div>
                <!-- Event Registrants -->
                <div class="box mt40">
                  <p class="text-center">
                    <?php
                      $usercount_sql = "SELECT COUNT(*) as count FROM registrants WHERE events_id = '$id'";
                      $usercount_result = mysqli_query($conn, $usercount_sql);
                      $usercount_row = mysqli_fetch_array($usercount_result);

                      if ( $usercount_row['count'] > 1 ) {
                        echo '<span class="count" >' . $usercount_row['count'] . '</span> <br /><span class="reg">Registrants are attending</span>';
                      }
                      else {
                        echo '<span class="count" >' . $usercount_row['count'] . '</span> <br /><span class="reg">Registrant is attending</span>';
                      }

                      // Close database connection
                      mysqli_close($conn);
                    ?>

                </div>
              </div>
              <!-- End: Course Content Section -->
            </div>
          </div>
          <!-- End: 2-Column GRID -->

          </div>
        </div>
