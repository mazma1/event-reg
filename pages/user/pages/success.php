

<div class="container">
  <div class="jumbotron col-md-6 col-md-offset-3 success">
    <h2>Success!</h2>
    <p>
      Thank you for completing the form. Your registration was submitted successful.
    </p>
    <a class="btn btn-default" href="?page=form&event_id=<?php echo $id; ?>" role="button">Back</a>

  </div>
</div>
