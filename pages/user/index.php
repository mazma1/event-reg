<?php
  include("../../includes/config.php");
 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Event Registration</title>

    <!-- Start: Header Scripts and CSS Files -->
    <?php
      include("common/header-scripts.php");
     ?>
    <!-- End: Header Scripts and CSS Files -->

  </head>

  <body>

    <?php
      if ($_GET['page'] == 'form' || $_GET['page'] == '') {
        include ('pages/form.php');
      }
      elseif ($_GET['page'] == 'success') {
        include ('pages/success.php');
      }
    ?>

  </body>
</html>
