<nav class="navbar navbar-default">
  <div class="container-fluid">

    <div class="navbar-header">
      <button type="button" class="navbar-toggle">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar bar1"></span>
          <span class="icon-bar bar2"></span>
          <span class="icon-bar bar3"></span>
      </button>
      <a class="navbar-brand" href="#">Admin</a>
    </div>

    <div class="collapse navbar-collapse">

      <ul class="nav navbar-nav navbar-right">
        <!--
        <li>
          <a href="#"> <i class="ti-settings"></i>
            <p>Settings</p>
          </a>
        </li>

        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="ti-settings"></i>
            <p>Settings</p>
            <b class="caret"></b>
          </a>
          <ul class="dropdown-menu">
            <li><a href="#industry&id=1">Add Industry</a></li>
            <li><a href="#">Add Interest</a></li>
          </ul>
        </li>
      -->
        <li>
          <a href="?page=logout"> <i class="fa fa-sign-out fa-lg" aria-hidden="true"></i>
            <p>Log Out</p>
          </a>
        </li>
      </ul>

    </div>
  </div>
</nav>
