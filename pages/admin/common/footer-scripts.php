<!-- Scripts -->

<!--   Core JS Files   -->
<script src="../../admin-resources/vendor/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="../../admin-resources/vendor/assets/js/bootstrap.min.js" type="text/javascript"></script>

<!-- Paper Dashboard Core javascript and methods for Demo purpose -->
<script src="../../admin-resources/vendor/assets/js/paper-dashboard.js"></script>

<!--  Notifications Plugin    -->
<script src="../../admin-resources/vendor/assets/js/bootstrap-notify.js"></script>


<!--  Custom JS    -->
<script src="../../admin-resources/vendor/assets/js/custom.js"></script>
