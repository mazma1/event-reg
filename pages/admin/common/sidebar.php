
  <!-- Start: Side Bar -->
  <div class="sidebar" data-background-color="white" data-active-color="danger">
    <div class="sidebar-wrapper">
      <div class="logo">
          <a href="" class="simple-text">
              Event Reg
          </a>
      </div>

      <ul class="nav">
        <li <?php if ($_GET['page'] == 'dashboard' || $_GET['page'] == '' ) { echo 'class="active"'; } ?>>
            <a href="?page=dashboard&p=1">
                <i class="ti-panel"></i>
                <p>Dashboard</p>
            </a>
        </li>

        <li <?php if ($_GET['page'] == 'users' ) { echo 'class="active"'; } ?>>
            <a href="?page=users&p=1">
                <i class="ti-user"></i>
                <p>Users</p>
            </a>
        </li>

        <li <?php if ($_GET['page'] == 'industry' ) { echo 'class="active"'; } ?>>
            <a href="?page=industry&p=1">
                <i class="ti-settings"></i>
                <p>Industries</p>
            </a>
        </li>

        <li <?php if ($_GET['page'] == 'interest' ) { echo 'class="active"'; } ?>>
            <a href="?page=interest&p=1">
                <i class="ti-light-bulb"></i>
                <p>Interests</p>
            </a>
        </li>

        <li <?php if ($_GET['page'] == 'attendance' ) { echo 'class="active"'; } ?>>
            <a href="">
                <i class="ti-pencil-alt"></i>
                <p>Attendance</p>
            </a>
        </li>
      </ul>

    </div>
  </div>
  <!-- End: Side Bar -->
