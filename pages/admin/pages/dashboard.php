<div class="wrapper">

  <!-- Start: Side Bar -->
  <?php
  include ('common/sidebar.php');
  ?>
  <!-- End: Side Bar -->

  <?php require("../../includes/db-connectvars.php"); ?>

  <!-- Start: Main Panel -->
  <div class="main-panel">

    <!-- Start: Nav Bar -->
      <?php include ('common/navbar.php') ?>
    <!-- End: Nav Bar -->

    <!-- Start: Main Content -->
    <div class="content">

      <div class="container-fluid">
        <div class="row">

          <!-- Start: Display successful event addition msg-->
          <?php if(isset($_SESSION['success'])) : ?>
            <div class="alert alert-info" style="margin: 0 16px 20px 16px;">
                <button type="button" aria-hidden="true" class="close">×</button>
                <span><?php echo $_SESSION['success']; ?></span>
            </div>
            <?php unset($_SESSION['success']);?>
          <?php endif;  ?>
          <!-- End: Display successful event addition msg -->

          <!-- Start: Event Count -->
          <div class="col-md-4">
            <div class="card">
              <div class="content">
                <?php
                  $event_sql = "SELECT COUNT(*) as count FROM events";
                  $event_result = mysqli_query($conn, $event_sql);
                  $event_row = mysqli_fetch_array($event_result);

                  if ( $event_row['count'] > 1 ) {
                    echo "<div class='dashboard-count'>" . $event_row['count'] . "</div>";
                    echo "<div>
                            <hr class='hr-dashboard' />
                            <div class='tag'>Events</div>
                          </div>";
                  }
                  else {
                    echo "<div class='dashboard-count'>" . $event_row['count'] . "</div>";
                    echo "<div>
                            <hr class='hr-dashboard' />
                            <div class='tag'>Event</div>
                          </div>";
                  }
                ?>
              </div>
            </div>
          </div>
          <!-- End: Event Count -->

          <!-- Start: Users Count -->
          <div class="col-md-4">
            <div class="card">
              <div class="content">
                <?php
                  $user_sql = "SELECT COUNT(*) as count FROM users";
                  $user_result = mysqli_query($conn, $user_sql);
                  $user_row = mysqli_fetch_array($user_result);

                  if ( $user_row['count'] > 1 ) {
                    echo "<div class='dashboard-count'>" . $user_row['count'] . "</div>";
                    echo "<div>
                            <hr class='hr-dashboard' />
                            <div class='tag'>Users</div>
                          </div>";
                  }
                  else {
                    echo "<div class='dashboard-count'>" . $user_row['count'] . "</div>";
                    echo "<div>
                            <hr class='hr-dashboard' />
                            <div class='tag'>User</div>
                          </div>";
                  }
                ?>
              </div>
            </div>
          </div>
          <!-- End: Users Count -->

          <!-- Start: Industries Count -->
          <div class="col-md-4">
            <div class="card">
              <div class="content">
                <?php
                  $industries_sql = "SELECT COUNT(*) as count FROM industry";
                  $industries_result = mysqli_query($conn, $industries_sql);
                  $industries_row = mysqli_fetch_array($industries_result);

                  if ( $industries_row['count'] > 1 ) {
                    echo "<div class='dashboard-count'>" . $industries_row['count'] . "</div>";
                    echo "<div>
                            <hr class='hr-dashboard' />
                            <div class='tag'>Industries</div>
                          </div>";
                  }
                  else {
                    echo "<div class='dashboard-count'>" . $industries_row['count'] . "</div>";
                    echo "<div>
                            <hr class='hr-dashboard' />
                            <div class='tag'>Industry</div>
                          </div>";
                  }
                ?>
              </div>
            </div>
          </div>
          <!-- End: Industries Count -->

          <!-- Start: Events Table-->
          <div class="col-md-12">
            <div class="card">
              <div class="header">
                <div class="row">
                    <div class="col-sm-10">
                      <h4 class="title" style="font-weight:500; padding-top: 12px;">EVENTS</h4>
                    </div>
                    <div class="col-sm-2">
                      <!--<button type="submit" class="btn btn-info btn-fill dashboard-btn" href=" ">Add Event</button>-->
                      <a class="btn btn-info btn-fill dashboard-btn" href="?page=event" role="button">Add Event</a>
                    </div>
                </div>
              </div>
                <div class="content table-responsive table-full-width mt24">
                  <table class="table table-striped">
                    <thead>
                      <th width="5%">S/N</th>
                      <th width="35%">Event Title</th>
                      <th width="10%">Date</th>
                      <th width="7%">Registrants</th>
                      <th width="10%">Action</th>
                    </thead>
                    <tbody>
                      <?php

                        //$s_n = 1;
                        include ('common/pagination.php');

                        // Retrieve the list of Events and Registrants from the database
                        $usercount_sql = "SELECT *, count(events_id) as count
                                          FROM events
                                          LEFT JOIN registrants
                                          ON events.id = registrants.events_id
                                          GROUP BY event
                                          ORDER BY events.id ASC
                                          LIMIT $start_from, $num_rec_per_page";
                        $usercount_data = mysqli_query($conn, $usercount_sql);
                        //$usercount_row = mysqli_fetch_array($usercount_sql);

                        //Add each row of the events and registrants table to the dashboard
                        while ($usercount_row = mysqli_fetch_array($usercount_data)) { ?>
                          <tr>
                              <td><?php echo $s_n; ?></td>
                              <td> <a href = '/event/event-reg/pages/user/?event_id=<?php echo $usercount_row['id'];?> 'target='_blank' ><?php echo
                              $usercount_row['event']; ?></a> </td>
                              <td><?php echo $usercount_row['date']; ?></td>
                              <td> <a href= '/event/event-reg/pages/admin/?page=registrant&event_id=<?php echo $usercount_row['id']; ?>' ><?php echo $usercount_row['count'] ?></a> </td>
                              <td>
                                <a class='btn btn-info btn-fill dashboard-btn' href='?page=dashboard-view' role='button' style='padding: 3px 5px 3px 5px;'>View</a>
                                <a class='btn btn-info btn-fill dashboard-btn' href='?page=dashboard-view' role='button' style='padding: 3px 5px 3px 5px;'>Delete</a>
                              </td>
                           </tr>
                           <?php $s_n++;
                        }

                      ?>

                    </tbody>
                  </table>

                </div>
            </div>

            <!-- Start: Page Links -->
            <?php
              $usercount_sql = "SELECT *, count(events_id) as count
                                FROM events
                                LEFT JOIN registrants
                                ON events.id = registrants.events_id
                                GROUP BY event
                                ORDER BY events.id ASC";
              $usercount_data = mysqli_query($conn, $usercount_sql);

              $total_records = mysqli_num_rows($usercount_data);
              $total_pages = ceil($total_records / $num_rec_per_page);
            ?>

            <?php
              $count_sql = "SELECT COUNT(*) as count FROM events";
              $count_result = mysqli_query($conn, $count_sql);
              $count_row = mysqli_fetch_array($count_result);

              if ( $count_row['count'] > $num_rec_per_page ) { ?>
                <nav aria-label="Page navigation" class="text-center">
              <ul class="pagination" style="margin: auto;">
                <li><?php echo
                  "<a href='?page=dashboard&p=1' aria-label='Previous'>" .
                    '<span aria-hidden="true">&laquo;</span>' .
                  "</a>" ;?>
                </li>

                <?php
                for ($i=1; $i <= $total_pages; $i++) { ?>
                  <li <?php if ($_GET['p'] == $i ) { echo 'class="active"'; } ?>><?php echo
                    "<a href='?page=dashboard&p=".$i."'>".$i."</a> "; ?>
                  </li>
                <?php } ?>

                <li><?php echo
                  "<a href='?page=dashboard&p=$total_pages' aria-label='Next'>" .
                    '<span aria-hidden="true">&raquo;</span>' .
                  "</a>" ;?>
                </li>
              </ul>
            </nav>
            <?php } ?>
            <!-- End: Page Links -->

          </div>
          <!-- End: Events Table-->

          <!--Close database connection -->
          <?php mysqli_close($conn); ?>
        </div>
      </div>
    </div>
    <!-- End: Main Content -->


    <!-- Start: Footer -->
    <?php include ('common/footer.php'); ?>
    <!-- End: Footer -->


    </div>
  <!-- End: Main Panel -->


</div>
