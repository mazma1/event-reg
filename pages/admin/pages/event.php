<?php
  // Connect to the database
  require("../../includes/db-connectvars.php");

  // --SUBMITTING USER DATA TO DATABASE--
  // define variables and set to empty values
  $event_date = $event_title = $event_desc = "";
  $event_dateErr = $event_titleErr = $event_descErr =  "";
  $errors = array();


  // If add event form has been submitted...
  if (isset($_POST['submit'])) {
    //Collect Data submitted by user
    $event_date = $_POST["event-date"];
    $event_title = $_POST["event-title"];
    $event_desc = $_POST["event-desc"];
    $event_cost = $_POST["cost"];
    $event_form = false; //FLAG

    // --CHECK IF THE FORM FIELDS WERE FILLED--
    // Event Date
    if (empty($event_date)) {
      $event_dateErr = "* Please enter the prospective event date";
    }
    $event_form = true;

    // Event Title
    if (empty($event_title)) {
      $event_titleErr = "* Please enter the event title";
    }
    $event_form = true;

    // Event Description
    if (empty($event_desc)) {
      $event_descErr = "* Please enter event description";
    }
    $event_form = true;

    //If Event Banner is uploaded
    if (isset($_FILES['banner'])) {

      $event_banner = $_FILES['banner']['name'];
      $file_size = $_FILES['banner']['size'];
      $file_tmp = $_FILES['banner']['tmp_name'];

      $file_ext = strtolower(end(explode('.', $_FILES['banner']['name'])));

      //Define the extensions that can be uploaded
      $extensions = array("jpeg", "jpg", "png");

      /*
         Check if the uploaded file's extension matches what has been defined by looking
         into the array using the in_array() function
      */
      if (!empty($event_banner) && in_array($file_ext, $extensions) === false ) {
         $errors[] = "* Image format not allowed, please choose a JPEG or PNG file.";
      }
      $event_form = true;

      //Check the size of the uploaded file and notify user if it exceeds 1MB
      if ($file_size > 1048576) {
         $errors[] = "* File size must not be more than 1 MB";
      }
      $event_form = true;

      //End Event Banner error check


      // Upload data
      //If all required fields are without error (with banner)
      if (empty($event_dateErr) && empty($event_titleErr) && empty($event_descErr) && !empty($event_banner) && empty($errors)) {

        //Move uploaded banner to target folder
        if (move_uploaded_file($file_tmp, "pages/upload/".$event_banner)) {

          // If no error, enter the data into the database
          $submit_sql = "INSERT INTO events (event, date, event_desc, banner, cost) VALUES ('$event_title', '$event_date', '$event_desc', '$event_banner', '$event_cost' )";

          mysqli_query($conn, $submit_sql);

          $_SESSION['success'] = "Event successfully added!";


          $success_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/?page=dashboard';
          header('Location: ' . $success_url);
          exit;

          $event_date = $event_title = "";
        }

        else { ?>
          <div class="alert alert-info" style="margin: 0 16px 20px 16px;">
              <button type="button" aria-hidden="true" class="close">×</button>
              <span><?php echo "There was a problem uploading the file "; ?></span>
          </div>
        <?php
        }
      }
    }

    // If no banner
    elseif (!isset($_FILES['banner'])) {

      if( empty($event_dateErr) && empty($event_titleErr) && empty($event_descErr) ) {

        $submit_sql = "INSERT INTO events (event, date, event_desc, cost) VALUES ('$event_title', '$event_date', '$event_desc', '$event_cost' )";

        mysqli_query($conn, $submit_sql);

        $_SESSION['success'] = "Event successfully added!";


        $success_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/?page=dashboard';
        header('Location: ' . $success_url);
        exit;
        $event_date = $event_title = "";
      }
    }
  }


  //...else if the form has not been submitted, display the form
  else {
    $event_form = true;
  }

?>


<div class="wrapper">

  <!-- Start: Side Bar -->
  <?php
  include ('common/sidebar.php');
  ?>
  <!-- End: Side Bar -->

  <!-- Start: Main Panel -->
  <div class="main-panel">

    <!-- Start: Nav Bar -->
      <?php include ('common/navbar.php') ?>
    <!-- End: Nav Bar -->

    <!-- Start: Main Content -->
    <div class="container-fluid login-wrapper">
      <div class="row">

        <?php if ($event_form) { ?>

         <!--Start: Add Event Form -->
         <div class="col-lg-8 col-lg-offset-2 col-md-7">
           <div class="card" style="border: 1px solid #ccc5b9;">
             <div class="header">
               <h4 class="title">Add Event</h4>
             </div>

             <div class="content">
                 <form method="post" action="?page=event" enctype="multipart/form-data">
                     <div class="row">
                         <div class="col-md-12">
                             <div class="form-group">
                                 <label>Event Date</label><span style="color: red;"><?php echo $event_dateErr; ?></span>
                                 <input type="date" name="event-date" class="form-control border-input" placeholder="dd-mm-yyyy" value="<?php echo $event_date;?>">
                             </div>
                         </div>
                     </div>

                     <div class="row">
                         <div class="col-md-12">
                             <div class="form-group">
                                 <label>Event Title</label><span style="color: red;"><?php echo $event_titleErr; ?></span>
                                 <input type="Text" name="event-title" class="form-control border-input" placeholder="Event Title" value="<?php echo $event_title;?>">
                             </div>
                         </div>
                     </div>

                     <div class="row">
                         <div class="col-md-12">
                             <div class="form-group">
                                 <label>Event Description</label><span style="color: red;"><?php echo $event_descErr; ?></span>
                                 <textarea rows="5" name="event-desc" class="form-control border-input" placeholder="Brief description of event"><?php if (isset($event_desc)) { echo $event_desc; } ?></textarea>
                             </div>
                         </div>
                     </div>

                     <div class="row">
                         <div class="col-md-12">
                             <div class="form-group">
                                <label>Event Banner</label><span style="color: red;"><?php echo implode('\n', $errors); ?>
                                <input type="file" name="banner" class="form-control border-input">
                             </div>
                         </div>
                     </div>

                     <div class="row">
                         <div class="col-md-12">
                             <div class="form-group">
                                 <label>Cost (&#8358;)</label>
                                 <input type="text" name="cost" class="form-control border-input" placeholder="Optional" value="<?php echo $event_cost;?>">
                             </div>
                         </div>
                     </div>

                     <div class="text-center">
                         <button type="submit" name="submit" class="btn btn-info btn-fill dashboard-btn">Submit</button>
                     </div>
                     <div class="clearfix"></div>
                 </form>
             </div>
          </div>
        </div>
        <?php } ?>
         <!-- End: Add Event Form-->


      </div>
    </div>
    <!-- End: Main Content -->

    <!-- Start: Footer -->
    <?php include ('common/footer.php'); ?>
    <!-- End: Footer -->

  </div>
  <!-- End: Main Panel -->

</div>
