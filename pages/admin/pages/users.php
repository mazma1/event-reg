<?php

  $id = $_GET['event_id'];

  //$s_n = 1; // Serial Number Variable

  require("../../includes/db-connectvars.php");

  include ('common/pagination.php');

  // Retrieve the list of users and events they registered for
  $usercount_sql = " SELECT *, count(events_id) as count
                    FROM users LEFT JOIN registrants
                    ON users.id = registrants.users_id
                    GROUP BY users.id
                    ORDER BY users.first_name ASC
                    LIMIT $start_from, $num_rec_per_page";

  $usercount_result = mysqli_query($conn, $usercount_sql);


?>

<div class="wrapper">

  <!-- Start: Side Bar -->
  <?php
  include ('common/sidebar.php');
  ?>
  <!-- End: Side Bar -->

  <!-- Start: Main Panel -->
  <div class="main-panel">

    <!-- Start: Nav Bar -->
      <?php include ('common/navbar.php') ?>
    <!-- End: Nav Bar -->

    <!-- Start: Main Content -->
    <div class="content">

      <div class="container-fluid">
        <div class="row">

          <!-- Start: Users Table-->
          <div class="col-md-12">

            <div class="card">
              <div class="header">

                <div class="row">

                  <div class="col-sm-10">
                    <h4 class="title" style="font-weight:500; padding-top: 12px;">REGISTERED USERS</h4>
                  </div>

                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped mt56">
                          <thead>
                            <th>S/N</th>
                            <th>Name</th>
                            <th>Mobile Number</th>
                            <th>Email</th>
                            <th>Events Registered</th>
                            <th>Action</th>
                          </thead>
                          <tbody>

                            <?php

                              //Add each row of the events and registrants table to the dashboard
                              while ($usercount_row = mysqli_fetch_array($usercount_result)) { ?>
                                <tr>
                                  <td><?php echo $s_n; ?></td>
                                  <td><?php echo $usercount_row['first_name'] . " " . $usercount_row['last_name']; ?></td>
                                  <td><?php echo $usercount_row['mobile_number']; ?></td>
                                  <td><?php echo $usercount_row['email']; ?></td>
                                  <td><?php echo $usercount_row['count']; ?></td>
                                  <td>
                                    <a class='btn btn-info btn-fill dashboard-btn' href='/event/event-reg/pages/admin/?page=userview&id=<?php echo $usercount_row['id'];?>' role='button' style='padding: 3px 5px 3px 5px;'>View</a>

                                    <!--
                                    <a class='btn btn-info btn-fill dashboard-btn' href='/event/event-reg/pages/admin/?page=edit&user=<?php //echo $regcount_row['id']; ?>&event_id=<?php //echo $id; ?>' role='button' style='padding: 3px 5px 3px 5px;'>Edit</a>

                                    <a class='btn btn-info btn-fill dashboard-btn' href='/event/event-reg/pages/admin/?page=delete&user=<?php //echo $regcount_row['id']; ?>&event_id=<?php// echo $id; ?>' role='button' style='padding: 3px 5px 3px 5px;'>Delete</a>
                                  -->
                                  </td>
                                </tr>
                                <?php $s_n++;
                              }
                              ?>


                          </tbody>
                      </table>

                  </div>

                </div>

              </div>
            </div>

            <!-- Start: Page Links -->
            <?php
            $usercount_sql = " SELECT *, count(events_id) as count
                              FROM users LEFT JOIN registrants
                              ON users.id = registrants.users_id
                              GROUP BY users.id
                              ORDER BY users.first_name ASC";
            $usercount_result = mysqli_query($conn, $usercount_sql);

            $total_records = mysqli_num_rows($usercount_result);
            $total_pages = ceil($total_records / $num_rec_per_page); ?>

            <?php
              $count_sql = "SELECT COUNT(*) as count FROM events";
              $count_result = mysqli_query($conn, $count_sql);
              $count_row = mysqli_fetch_array($count_result);

              if ( $count_row['count'] > $num_rec_per_page ) { ?>
                <nav aria-label="Page navigation" class="text-center">
              <ul class="pagination" style="margin: auto;">
                <li><?php echo
                  "<a href='?page=users&p=1' aria-label='Previous'>" .
                    '<span aria-hidden="true">&laquo;</span>' .
                  "</a>" ;?>
                </li>

                <?php
                for ($i=1; $i <= $total_pages; $i++) { ?>
                  <li <?php if ($_GET['p'] == $i ) { echo 'class="active"'; } ?>><?php echo
                    "<a href='?page=users&p=".$i."'>".$i."</a> "; ?>
                  </li>
                <?php } ?>

                <li><?php echo
                  "<a href='?page=users&p=$total_pages' aria-label='Next'>" .
                    '<span aria-hidden="true">&raquo;</span>' .
                  "</a>" ;?>
                </li>
              </ul>
            </nav>
              <?php } ?>
            <!-- End: Page Links -->

            <!--<a class="btn btn-info btn-fill dashboard-btn" href="?page=dashboard" role="button">Back</a>-->
          </div>
          <!-- End: Userss Table-->

        </div>
      </div>
    </div>
    <!-- End: Main Content -->

    <!-- Start: Footer -->
    <?php include ('common/footer.php'); ?>
    <!-- End: Footer -->

  </div>

</div>
