<?php
  // Even when logging out, you start the session in order to access the session variables
  session_start();

  //Check the log in status. If the user is logged in, delete the session vars to log them out
  if (isset($_SESSION['username'])) {
    // Delete the session vars by clearing the $_SESSION array
    $_SESSION = array();

    // Destroy the session
    session_destroy();
  }

  // Redirect to the login page
  $login_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '?page=login';
  header('Location: ' . $login_url);
?>
