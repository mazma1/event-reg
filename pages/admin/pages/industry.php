  <?php require("../../includes/db-connectvars.php"); ?>

<!-- Start: Add Industry Script -->
<?php

  $new_ind = "";
  $error = "";

  //If the form has been submitted...
  if (isset($_POST['add_industry'])) {

    $new_ind = mysqli_real_escape_string($conn, trim($_POST['new_ind']));
    $form = false;

    if (empty($new_ind)) {
      $error = " * Please enter the new industry of choice";
      $form = true;
    }

    else {

      $industry_sql = "SELECT * FROM industry WHERE industry = '$new_ind'" ;
      $industry_result = mysqli_query($conn, $industry_sql);

      //If industry is not existing...
      if (mysqli_num_rows($industry_result) == 0) {

        $add_sql = "INSERT INTO  industry (industry) VALUES ('$new_ind')";

        $add_result = mysqli_query($conn, $add_sql);

        $_SESSION['add'] = "New industry successfully added!";

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;

        $new_ind = "";
        $form = true;
      }

      else{
        $error = " * This Industry already exists.";
        $form = true;
      }
    }
  }
  // If form has not been submitted
  else {
    $form = true;
  }
?>
<!-- End: Add Industry Script -->


<!-- Start: Delete Industry Script -->
<?php
  if(isset($_GET['action'])) {

    $id = $_GET['id'];
    $action = $_GET['action'];

    if ($action == 'delete'){

      // Industry Details SQL
      $industry_sql = " SELECT * FROM industry
                        WHERE id = '$id'";
      $industry_result = mysqli_query($conn, $industry_sql);

      while ($industry_row = mysqli_fetch_array($industry_result)) {

        $delete_sql = "DELETE FROM industry WHERE id = '$industry_row[id]'";
        $delete_result = mysqli_query($conn, $delete_sql);

        $_SESSION['delete'] = "Industry successfully deleted!";

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
      }
    }

  }
?>
<!-- End: Delete Industry Script -->


<!-- Start: Edit Industry Script -->
<?php

  $new_ind = "";
  $error = "";

  //If the form has been submitted...
  if (isset($_POST['edit_industry'])) {

    $new_ind = mysqli_real_escape_string($conn, trim($_POST['new_ind']));
    $id = mysqli_real_escape_string($conn, trim($_POST['industryId']));
    $form = false;

    if (empty($new_ind)) {
      $error = " * Please enter the new industry of choice";
      $form = true;
    }

    else {

      $industry_sql = "SELECT * FROM industry WHERE industry = '$new_ind'" ;
      $industry_result = mysqli_query($conn, $industry_sql);

      //If industry is not existing...
      if (mysqli_num_rows($industry_result) == 0) {

        $edit_sql = "UPDATE industry SET industry = '$new_ind' WHERE id = '$id'";
        $edit_result = mysqli_query($conn, $edit_sql);

        $_SESSION['edit'] = "Industry successfully edited!";

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;

        $new_ind = "";
        $form = true;
      }

      else{
        $error = " * This Industry already exists.";
        $form = true;
      }
    }
  }
  // If form has not been submitted
  else {
    $form = true;
  }
?>
<!-- End: Edit Industry Script -->



<div class="wrapper">

  <!-- Start: Side Bar -->
  <?php
  include ('common/sidebar.php');
  ?>
  <!-- End: Side Bar -->


  <!-- Start: Main Panel -->
  <div class="main-panel">

    <!-- Start: Nav Bar -->
      <?php include ('common/navbar.php') ?>
    <!-- End: Nav Bar -->

    <!-- Start: Main Content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">

          <!-- Start: Display successful industry addition msg-->
          <?php if(isset($_SESSION['add'])) : ?>
            <div class="alert alert-info" style="margin: 0 16px 20px 16px;">
                <button type="button" aria-hidden="true" class="close">×</button>
                <span><?php echo $_SESSION['add']; ?></span>
            </div>
            <?php unset($_SESSION['add']);?>
          <?php endif;  ?>
          <!-- End: Display successful industry addition msg -->


          <!-- Start: Display successful industry deletion msg-->
          <?php if(isset($_SESSION['delete'])) : ?>
            <div class="alert alert-info" style="margin: 0 16px 20px 16px;">
                <button type="button" aria-hidden="true" class="close">×</button>
                <span><?php echo $_SESSION['delete']; ?></span>
            </div>
            <?php unset($_SESSION['delete']);?>
          <?php endif;  ?>
          <!-- End: Display successful industry deletion msg -->


          <!-- Start: Display successful industry addition msg-->
          <?php if(isset($_SESSION['edit'])) : ?>
            <div class="alert alert-info" style="margin: 0 16px 20px 16px;">
                <button type="button" aria-hidden="true" class="close">×</button>
                <span><?php echo $_SESSION['edit']; ?></span>
            </div>
            <?php unset($_SESSION['edit']);?>
          <?php endif;  ?>
          <!-- End: Display successful industry addition msg -->


          <!-- Start: Industry Table-->
          <div class="col-md-8">
            <div class="card">
              <div class="header">
                <div class="row">
                    <div class="col-sm-12">
                      <h4 class="title" style="font-weight:500; padding-top: 12px;">INDUSTRIES</h4>
                    </div>

                </div>
              </div>

              <div class="content table-responsive table-full-width mt24">
                  <table class="table table-striped">
                    <thead>
                      <th width="6%">S/N</th>
                      <th width="60%">Industry</th>
                      <th width="16%">Action</th>
                    </thead>

                    <tbody>
                      <?php
                      //$s_n = 1;

                        include ('common/pagination.php');

                        // Retrieve the list of Events and Registrants from the database
                        $industry_sql = "SELECT * FROM industry ORDER BY id DESC  LIMIT $start_from, $num_rec_per_page";
                        $industry_result = mysqli_query($conn, $industry_sql);
                        //$usercount_row = mysqli_fetch_array($usercount_sql);

                        //Add each row of the events and registrants table to the dashboard
                        while ($industry_row = mysqli_fetch_array($industry_result)) {  ?>
                            <tr>
                                <td><?php echo $s_n; ?></td>
                                <td class='industryName'><?php echo $industry_row['industry']; ?></td>
                                <td class='industryId' style='display:none'><?php echo $industry_row['id']; ?></td>
                                <td>
                                  <button class='btn btn-info btn-fill dashboard-btn editBtn' href='?page=industry&id=<?php echo $industry_row['id']; ?>' role='button' style='padding: 3px 5px 3px 5px;'>Edit</button>

                                  <a class="btn btn-info btn-fill dashboard-btn" href="/event/event-reg/pages/admin/?page=industry&action=delete&id=<?php echo $industry_row['id']; ?>" role='button' style='padding: 3px 5px 3px 5px;' onclick="return confirm ('Are you sure you want to delete <?php echo $industry_row['industry']; ?>?');">Delete</a>
                                </td>
                            </tr>
                            <?php $s_n++;
                        } ?>

                    </tbody>
                  </table>
                </div>
            </div>


            <!-- Start: Page Links -->
            <?php
            $industry_sql = "SELECT * FROM industry ORDER BY id DESC";
            $industry_result = mysqli_query($conn, $industry_sql);

            $total_records = mysqli_num_rows($industry_result);
            $total_pages = ceil($total_records / $num_rec_per_page); ?>

            <?php
              $count_sql = "SELECT COUNT(*) as count FROM industry";
              $count_result = mysqli_query($conn, $count_sql);
              $count_row = mysqli_fetch_array($count_result);

              if ( $count_row['count'] > $num_rec_per_page ) { ?>
                <nav aria-label="Page navigation" class="text-center">
              <ul class="pagination" style="margin: auto;">
                <li><?php echo
                  "<a href='?page=industry&p=1' aria-label='Previous'>" .
                    '<span aria-hidden="true">&laquo;</span>' .
                  "</a>" ;?>
                </li>

                <?php
                for ($i=1; $i <= $total_pages; $i++) { ?>
                  <li <?php if ($_GET['p'] == $i ) { echo 'class="active"'; } ?>><?php echo
                    "<a href='?page=industry&p=".$i."'>".$i."</a> "; ?>
                  </li>
                <?php } ?>

                <li><?php echo
                  "<a href='?page=industry&p=$total_pages' aria-label='Next'>" .
                    '<span aria-hidden="true">&raquo;</span>' .
                  "</a>" ;?>
                </li>
              </ul>
            </nav>
              <?php } ?>
            <!-- End: Page Links -->

          </div>
          <!-- End: Industry Table-->



          <!-- Start: Add Industry / Edit Industry-->
          <?php if ($form) { ?>

          <div class="col-md-4">
              <div class="card">
                <div class="header">
                  <h4 class="title" id="industryTitle">Add Industry</h4>
                </div>
                <hr style="margin: 8px 0 0;">

                <div class="content">
                  <form method="post" action="?page=industry">

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <!--<label>Event Date</label><span style="color: red;"><?php //echo $event_dateErr; ?></span>-->
                          <span style="color: red;"><?php echo $error; ?></span>
                          <input type="text" name="new_ind" class="form-control border-input" placeholder="Enter new industry" value="" class="mb16" style="margin-bottom: 16px;">

                          <button type="submit" id="industryBtn" name="add_industry" class="btn btn-fill btn-info btn-sm" style="
                          border-radius: 0;">Submit</button>
                          <input type="hidden" name="industryId" value="0" />
                        </div>
                      </div>
                    </div>
                </form>
              </div>
            </div>


          </div>
          <?php } ?>
          <!-- End: Add Industry -->

          <!--Close database connection -->
          <?php mysqli_close($conn); ?>
        </div>
      </div>
    </div>
    <!-- End: Main Content -->


    <!-- Start: Footer -->
    <?php include ('common/footer.php'); ?>
    <!-- End: Footer -->

  </div>
  <!-- End: Main Panel -->
</div>
