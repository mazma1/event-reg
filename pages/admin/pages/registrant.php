<?php

  $id = $_GET['event_id'];

  $s_n = 1; // Serial Number Variable

  require("../../includes/db-connectvars.php");

  // Retrieve the list of Events and Registrants from the database
  $regcount_sql = " SELECT *
                    FROM users
                    JOIN registrants
                    ON users.id = registrants.users_id
                    WHERE registrants.events_id = '$id'";

  $regcount_data = mysqli_query($conn, $regcount_sql);

?>

<div class="wrapper">

  <!-- Start: Side Bar -->
  <?php
  include ('common/sidebar.php');
  ?>
  <!-- End: Side Bar -->

  <!-- Start: Main Panel -->
  <div class="main-panel">

    <!-- Start: Nav Bar -->
      <?php include ('common/navbar.php') ?>
    <!-- End: Nav Bar -->

    <!-- Start: Main Content -->
    <div class="content">

      <div class="container-fluid">
        <div class="row">

          <!-- Start: Display successful removal of a user-->
          <?php if(isset($_SESSION['delete'])) : ?>
            <div class="alert alert-success" style="margin: 0 16px 20px 16px;">
                <!--<button type="button" aria-hidden="true" class="close">×</button>-->
                <span><?php echo $_SESSION['delete']; ?></span>
            </div>
            <?php unset($_SESSION['delete']);?>
          <?php endif;  ?>
          <!-- End: Display successful removal of a user -->


          <!-- Start: Display successful editing of a user's profile-->
          <?php if(isset($_SESSION['edit'])) : ?>
            <div class="alert alert-success" style="margin: 0 16px 20px 16px;">
                <!--<button type="button" aria-hidden="true" class="close">×</button>-->
                <span><?php echo $_SESSION['edit']; ?></span>
            </div>
            <?php unset($_SESSION['edit']);?>
          <?php endif;  ?>
          <!-- End: Display successful editing of a user's profile -->

          <!-- Start: Registrants Table-->
          <div class="col-md-8">

            <div class="card">
              <div class="header">

                <div class="row">

                  <div class="col-sm-10">
                    <h4 class="title" style="font-weight:500; padding-top: 12px;">REGISTRANTS</h4>
                  </div>

                  <div class="content table-responsive table-full-width">
                      <table class="table table-striped mt56">
                          <thead>
                            <th>S/N</th>
                            <th>Name</th>
                            <th>Mobile Num</th>
                            <th>Email</th>
                            <th>Action</th>
                          </thead>
                          <tbody>

                            <?php
                              //Add each row of the events and registrants table to the dashboard
                              while ($regcount_row = mysqli_fetch_array($regcount_data)) { ?>
                                <tr>
                                  <td><?php echo $s_n; ?></td>
                                  <td><?php echo $regcount_row['first_name'] . " " . $regcount_row['last_name']; ?></td>
                                  <td><?php echo $regcount_row['mobile_number']; ?></td>
                                  <td><?php echo $regcount_row['email']; ?></td>
                                  <td>
                                    <a class='btn btn-info btn-fill dashboard-btn' href='/event/event-reg/pages/admin/?page=view&user=<?php echo $regcount_row['id'];?>&event_id=<?php echo $id; ?>' role='button' style='padding: 3px 5px 3px 5px;'>View</a>

                                    <a class='btn btn-info btn-fill dashboard-btn' href='/event/event-reg/pages/admin/?page=edit&user=<?php echo $regcount_row['id']; ?>&event_id=<?php echo $id; ?>' role='button' style='padding: 3px 5px 3px 5px;'>Edit</a>

                                    <a class='btn btn-info btn-fill dashboard-btn' href='/event/event-reg/pages/admin/?page=delete&user=<?php echo $regcount_row['id']; ?>&event_id=<?php echo $id; ?>' role='button' style='padding: 3px 5px 3px 5px;'>Delete</a>
                                  </td>
                                </tr>
                                <?php $s_n++;
                              }
                              ?>


                          </tbody>
                      </table>

                  </div>

                </div>

              </div>
            </div>
            <a class="btn btn-info btn-fill dashboard-btn" href="?page=dashboard&p=1" role="button">Back</a>
          </div>
          <!-- End: Registrants Table-->

          <!--Start: Event Details -->
          <div class="col-md-4">

            <div class="card">
              <div class="header">
                <h4 class="title" style="font-weight:500; padding-top: 12px;">Event Details</h4>
              </div>
              <hr style="margin: 8px 0 0;">

              <div class="content">
                <?php
                  $event_sql = " SELECT *
                                FROM events
                                WHERE id = '$id'";
                  $event_data = mysqli_query($conn, $event_sql);

                  while ($event_row = mysqli_fetch_array($event_data)) {
                ?>
                <div class="pt0">
                  <p><strong>Title:</strong></p>
                  <p><?php echo $event_row['event']; ?></p>
                </div>

                <div class="pt16">
                  <p><strong>Date:</strong></p>
                  <p><?php echo $event_row['date']; ?></p>
                </div>
              </div>
              <?php } ?>


            </div>
          </div>
          <!-- End: Event Details -->
        </div>
      </div>
    </div>
    <!-- End: Main Content -->

    <!-- Start: Footer -->
    <?php include ('common/footer.php'); ?>
    <!-- End: Footer -->

  </div>

</div>
