<?php

  $user = $_GET['user'];
  $event = $_GET['event_id'];

  require("../../includes/db-connectvars.php");

  // define variables and set to empty values
  $first_name = $last_name = $email = $mobile_number = $industry  = "";
  $first_nameErr = $last_nameErr = $emailErr = $mobile_numberErr = $industryErr = "";


  // User Details SQL
  $userdetail_sql = "SELECT * FROM users
                     WHERE id = '$user'";
  $userdetail_result = mysqli_query($conn, $userdetail_sql);
?>


<?php
  //if form has been submitted, process the form contents...
  if (isset($_POST['edit_submit'])) {

    // Grab the profile data from the POST
    $first_name = mysqli_real_escape_string($conn, trim($_POST['firstname']));
    $last_name = mysqli_real_escape_string($conn, trim($_POST['lastname']));
    $mobile_number = mysqli_real_escape_string($conn, trim($_POST['mobile']));
    $email = mysqli_real_escape_string($conn, trim($_POST['email']));
    $industry = mysqli_real_escape_string($conn, trim($_POST['industry']));
    $output_form = false;

    // First Name
    if (empty($first_name)) {
      $first_nameErr = "* First Name is required";
    }
    $output_form = true;

    // Last Name
    if (empty($last_name)) {
      $last_nameErr = "* Last Name is required";
    }
    $output_form = true;

    // Email
    if (empty($email)) {
      $emailErr = "* Email is required";
    }
    $output_form = true;

    // Mobile Number
    if (empty($mobile_number)) {
      $mobile_numberErr = "* Please enter your mobile number";
    }
    else {
      $mobile_number = test_input($mobile_number);
      // check if mobile number contains only numbers
      if ( !is_numeric($mobile_number) ) {
        $mobile_numberErr = "Please enter a DIGITS only valid mobile number";
      }
    }
    $output_form = true;

    //Industry
    if (empty($industry)) {
      $industryErr = "* Please select your Industry of Operation";
    }
    $output_form = true;

    // if everything is fine, update the record in the database
    if ( empty($first_nameErr) && empty($last_nameErr) && empty($emailErr) && empty($mobile_numberErr) && empty($industryErr) ) {
      $update_sql = "UPDATE users
                     SET first_name = '$first_name', last_name = '$last_name', mobile_number = '$mobile_number',  email = '$email', industry = '$indstry'
                     WHERE id = '$user'";

      $update_result = mysqli_query($conn, $update_sql);

      $_SESSION['edit'] = "User's profile successfully updated!";


      // Confirm success with the user
      // Redirect to the registrants page
      $success_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/?page=registrant&event_id=' . $event;
      header('Location: ' . $success_url);
      exit;
      $first_name = $last_name = $email = $mobile_number = $industry = "";
    }
  }
  //...else if the form has not been submitted, display the form
  else {
    $output_form = true;
  }
?>

<div class="wrapper">

  <!-- Start: Side Bar -->
  <?php
  include ('common/sidebar.php');
  ?>
  <!-- End: Side Bar -->

  <!-- Start: Main Panel -->
  <div class="main-panel">

    <!-- Start: Nav Bar -->
      <?php include ('common/navbar.php') ?>
    <!-- End: Nav Bar -->

    <!-- Start: Main Content -->
    <div class="content">

      <div class="container-fluid">
        <div class="row">

          <div class="col-lg-8 col-lg-offset-2 col-md-7">
            <div class="card">
              <div class="header">
                  <h4 class="title">Edit Profile</h4>
              </div>


              <div class="content">
                <?php if ($output_form) { ?>

                  <form class="" action="?page=edit&user=<?php echo $user;?>&event_id=<?php echo $event;?>" method="post">
                  <?php while ($userdetail_row = mysqli_fetch_array($userdetail_result)) { ?>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>First Name</label><span class="error"><?php echo $first_nameErr; ?></span>
                          <input type="text" name="firstname" class="form-control border-input" placeholder="First Name" value="<?php echo $userdetail_row['first_name']; ?>">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Last Name</label><span class="error"><?php echo $last_nameErr; ?></span>
                          <input type="text" name="lastname"class="form-control border-input" placeholder="Last Name" value="<?php echo $userdetail_row['last_name']; ?>">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Mobile Number</label><span class="error"><?php echo $mobile_numberErr; ?></span>
                          <input type="text" name="mobile" class="form-control border-input" placeholder="Mobile Number" value="<?php echo $userdetail_row['mobile_number']; ?>">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Email</label><span class="error"><?php echo $emailErr; ?></span>
                          <input type="text" name="email" class="form-control border-input" placeholder="Email" value="<?php echo $userdetail_row['email']; ?>">
                        </div>
                      </div>
                    </div>
                  <?php } ?>


                  <!-- Display the user's industry -->
                  <?php

                  $industry_sql = " SELECT *
                                    FROM users
                                    JOIN industry
                                    ON users.industry = industry.id
                                    WHERE users.id = '$user'";

                  $industry_data = mysqli_query($conn, $industry_sql); ?>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form">Industry</label><span class="error"><?php echo $industryErr; ?></span>

                          <select name="industry" class=" form-control border-input mb32">

                            <?php while ($industry_row = mysqli_fetch_array($industry_data)) { ?>
                            <option value="<?php echo $industry_row['industry'];?>">- Category
                               -</option>
                            ?>
                              <?php
                                //Add each row of the industry table to the options

                                // Retrieve the list of INDUSTRIES from the database for Industries options
                                $industry_list_sql = "SELECT * FROM industry ORDER BY industry ASC";
                                $industry_list_result = mysqli_query($conn, $industry_list_sql);

                                while ($industry_list_row = mysqli_fetch_array($industry_list_result)) {
                                  echo '<option value =' . $industry_list_row['id'];
                                  if ( $industry_row['industry'] == $industry_list_row['industry'] ) {
                                    echo " selected";
                                  }
                                  echo '>' . $industry_list_row['industry'] . '</option>';
                                }
                              ?>
                          </select>
                          <?php } ?>
                      </div>
                    </div>
                  </div>

                  <div class="pt40 pb8">
                      <button type="submit" name="edit_submit" class="btn btn-info btn-fill" style="border-radius: 0;">Update Profile</button>
                      <a class="btn btn-danger btn-fill dashboard-btn" href="?page=registrant&event_id=<?php echo $event; ?>" role="button">Cancel</a>
                  </div>

                </form>

                <?php } ?>
              </div>

            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- End: Main content -->

    <!-- Start: Footer -->
    <?php include ('common/footer.php'); ?>
    <!-- End: Footer -->

  </div>
</div>
