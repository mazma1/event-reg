<?php
  require("../../includes/db-connectvars.php");

  // Start the session
  //session_start();

  // Clear the error message
  $error_msg = "";

  // --------------------- If the user isn't logged in, try to log them in

  //If the user is not logged in...
  if (!isset($_SESSION['username'])) {
    //...and has submitted log-in data...
    if (isset($_POST['login-submit'])) {
      // Connect to the database and validate the data
      //$conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

      // Grab the user-entered log-in data
      $admin_username = mysqli_real_escape_string($conn, trim($_POST['username']));
      $admin_password = mysqli_real_escape_string($conn, trim($_POST['password']));

      if (!empty($admin_username) && !empty($admin_password)) {
        // Look up the username and password in the database
        $admin_query = "SELECT * FROM admin WHERE username = '$admin_username' AND password = SHA('$admin_password')";
        $admin_data = mysqli_query($conn, $admin_query);

        if (mysqli_num_rows($admin_data) == 1) {
          // The log-in is OK so set the username and password session variables and redirect to the
          $row = mysqli_fetch_array($admin_data);
          //Log in the user by setting username and password session variables
          $_SESSION['username'] = $row['username'];
          $_SESSION['password'] = $row['password'];
          $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/index.php';
          header('Location: ' . $home_url);
        }
        else {
          // The username/password are incorrect so set an error message
          $error_msg = 'Sorry, you must enter a valid username and password to log in.';
        }
      }
      else {
        // The username/password weren't entered so set an error message
        $error_msg = 'Sorry, you must enter your username and password to log in.';
      }
    }
  }
?>

<div class="wrapper">
  <div class="content">

    <!-- Start: Display login error message if any -->
    <?php if($error_msg) : ?>
      <div class="error">
        <?php echo $error_msg; ?>
      </div>
    <?php endif; ?>
    <!-- End: Display login error message if any -->

    <div class="login-wrapper">
      <form method="post" action="?page=login" class="login-form">
        <h3 class="mb32 text-center">Admin Log In</h3>

        <input type="text" class="form-con mb8" name="username" placeholder="Username" required="" autofocus="" />

        <input type="password" class="form-con mb40" name="password" placeholder="Password" required=""/>

        <div class="text-center">
          <button type="submit" name="login-submit" class="btn btn-info btn-fill btn-wd">Log In</button>
        </div>
      </form>
    </div>

  </div>
</div>
