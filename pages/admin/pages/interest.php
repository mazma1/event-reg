<?php require("../../includes/db-connectvars.php"); ?>


<!-- Start: Add Interest Script -->
<?php
  $new_int = "";
  $error = "";


  //If the form has been submitted...
  if (isset($_POST['submit'])) {

    $new_int = mysqli_real_escape_string($conn, trim($_POST['new_int']));
    $form = false;

    if (empty($new_int)) {
      $error = " * Please enter the new interest of choice";
      $form = true;
    }

    else {

      $interest_sql = "SELECT * FROM interest WHERE interest = '$new_int'" ;
      $interest_result = mysqli_query($conn, $interest_sql);

      //If industry is not existing...
      if (mysqli_num_rows($interest_result) == 0) {

        $add_sql = "INSERT INTO  interest (interest) VALUES ('$new_int')";
        $add_result = mysqli_query($conn, $add_sql);

        $_SESSION['add'] = "New interest successfully added!";

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;

        $new_ind = "";
        $form = true;
      }

      else{
        $error = " * This Industry already exists.";
        $form = true;
      }
    }

  }
  // If form has not been submitted
  else {
    $form = true;
  }
?>
<!-- End: Add Interest Script -->


<!-- Start: Delete Interest Script -->
<?php
  if(isset($_GET['action'])) {

    $id = $_GET['id'];
    $action = $_GET['action'];

    if ($action == 'delete'){
      echo $id;
      // Interest Details SQL
      $interest_sql = " SELECT * FROM interest
                        WHERE id = '$id'";
      $interest_result = mysqli_query($conn, $interest_sql);

      while ($interest_row = mysqli_fetch_array($interest_result)) {

        $delete_sql = "DELETE FROM interest WHERE id = '$interest_row[id]'";
        $delete_result = mysqli_query($conn, $delete_sql);

        $_SESSION['delete'] = "Interest successfully deleted!";

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
      }
    }

  }
?>
<!-- End: Delete Interest Script -->




<div class="wrapper">

  <!-- Start: Side Bar -->
  <?php
  include ('common/sidebar.php');
  ?>
  <!-- End: Side Bar -->

  <!-- Start: Main Panel -->
  <div class="main-panel">

    <!-- Start: Nav Bar -->
    <?php include ('common/navbar.php') ?>
    <!-- End: Nav Bar -->

    <!-- Start: Main Content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">

          <!-- Start: Display successful industry addition msg-->
          <?php if(isset($_SESSION['add'])) : ?>
            <div class="alert alert-info" style="margin: 0 16px 20px 16px;">
              <button type="button" aria-hidden="true" class="close">×</button>
              <span><?php echo $_SESSION['add']; ?></span>
            </div>
            <?php unset($_SESSION['add']);?>
          <?php endif;  ?>
          <!-- End: Display successful industry addition msg -->


          <!-- Start: Display successful industry deletion msg-->
          <?php if(isset($_SESSION['delete'])) : ?>
            <div class="alert alert-info" style="margin: 0 16px 20px 16px;">
              <button type="button" aria-hidden="true" class="close">×</button>
              <span><?php echo $_SESSION['delete']; ?></span>
            </div>
            <?php unset($_SESSION['delete']);?>
          <?php endif;  ?>
          <!-- End: Display successful industry deletion msg -->


          <!-- Start: Display successful industry edit msg-->
          <?php if(isset($_SESSION['edit'])) : ?>
            <div class="alert alert-info" style="margin: 0 16px 20px 16px;">
              <button type="button" aria-hidden="true" class="close">×</button>
              <span><?php echo $_SESSION['edit']; ?></span>
            </div>
            <?php unset($_SESSION['edit']);?>
          <?php endif;  ?>
          <!-- End: Display successful industry edit msg -->


          <!-- Start: Interest Table-->
          <div class="col-md-8">
            <div class="card">
              <div class="header">
                <div class="row">
                    <div class="col-sm-12">
                      <h4 class="title" style="font-weight:500; padding-top: 12px;">INTERESTS</h4>
                    </div>

                </div>
              </div>
                <div class="content table-responsive table-full-width mt24">
                  <table class="table table-striped">
                    <thead>
                      <th width="6%">S/N</th>
                      <th width="60%">Interest</th>
                      <th width="16%">Action</th>
                    </thead>
                    <tbody>
                      <?php

                      //$s_n = 1;
                      $p = $_GET['p'];
                      if (empty($p) || is_numeric($p) == false) {
                        $p = 1;
                      }
                      else {
                        $p = $_GET['p'];
                      }

                      $num_rec_per_page = 10;
                      $start_from = ($p-1) * $num_rec_per_page;

                      //Page Serial Number
                      if ($p > 1) {
                        $s_n = (($p * $num_rec_per_page) - 9);
                      }
                      else {
                        $s_n = 1;
                      }

                        // Retrieve the list of Events and Registrants from the database
                        $interest_sql = "SELECT * FROM interest ORDER BY id DESC LIMIT $start_from, $num_rec_per_page";
                        $interest_result = mysqli_query($conn, $interest_sql);
                        //$usercount_row = mysqli_fetch_array($usercount_sql);

                        //Add each row of the events and registrants table to the dashboard
                        while ($interest_row = mysqli_fetch_array($interest_result)) { ?>
                          <tr>
                            <td><?php echo $s_n; ?></td>
                            <td class="interestName"><?php echo $interest_row['interest']; ?></td>
                            <td class="interestId" style="display: none"><?php echo $interest_row['id']; ?></td>
                            <td>
                              <button class='btn btn-info btn-fill dashboard-btn editInterestBtn' role='button' style='padding: 3px 5px 3px 5px;'>Edit</button>

                              <a class="btn btn-info btn-fill dashboard-btn" href="/event/event-reg/pages/admin/?page=interest&action=delete&id=<?php echo $interest_row['id']; ?>" role='button' style='padding: 3px 5px 3px 5px;' onclick="return confirm ('Are you sure you want to delete this interest, <?php echo $interest_row['interest']; ?>?');">Delete</a>
                            </td>

                         </tr>
                          <?php $s_n++;
                        }

                      ?>

                    </tbody>
                  </table>

                </div>
            </div>


            <!-- Start: Page Links -->
            <?php
            $interest_sql = "SELECT * FROM interest ORDER BY id DESC";
            $interest_result = mysqli_query($conn, $interest_sql);

            $total_records = mysqli_num_rows($interest_result);
            $total_pages = ceil($total_records / $num_rec_per_page); ?>

            <?php
              $count_sql = "SELECT COUNT(*) as count FROM interest";
              $count_result = mysqli_query($conn, $count_sql);
              $count_row = mysqli_fetch_array($count_result);

              if ( $count_row['count'] > $num_rec_per_page) { ?>
                <nav aria-label="Page navigation" class="text-center">
                  <ul class="pagination" style="margin: auto;">
                    <li><?php echo
                      "<a href='?page=interest&p=1' aria-label='Previous'>" .
                        '<span aria-hidden="true">&laquo;</span>' .
                      "</a>" ;?>
                    </li>

                    <?php
                    for ($i=1; $i <= $total_pages; $i++) { ?>
                      <li <?php if ($_GET['p'] == $i ) { echo 'class="active"'; } ?>><?php echo
                        "<a href='?page=interest&p=".$i."'>".$i."</a> "; ?>
                      </li>
                    <?php } ?>

                    <li><?php echo
                      "<a href='?page=interest&p=$total_pages' aria-label='Next'>" .
                        '<span aria-hidden="true">&raquo;</span>' .
                      "</a>" ;?>
                    </li>
                  </ul>
                </nav>
              <?php } ?>
            <!-- End: Page Links -->

          </div>
          <!-- End: Industry Table-->

          <!-- Start: Add Industry -->
          <?php if ($form) { ?>

          <div class="col-md-4">
              <div class="card">
                <div class="header">
                  <h4 class="title" id="interestTitle">Add Interest</h4>
                </div>
                <hr style="margin: 8px 0 0;">

                <div class="content">
                  <form method="post" action="?page=interest">

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                            <!--<label>Event Date</label><span style="color: red;"><?php echo $event_dateErr; ?></span>-->
                            <span style="color: red;"><?php echo $error; ?></span>
                            <input type="text" name="new_int" class="form-control border-input" placeholder="Enter a new interest area" value="" class="mb16" style="margin-bottom: 16px;">

                            <button type="submit" name="submit" class="btn btn-fill btn-info btn-sm" style="
                            border-radius: 0;" id="interestBtn">Submit</button>
                            <input type="hidden" value="0" name="interestId"/>
                        </div>
                      </div>
                    </div>

                </form>
              </div>
            </div>

          </div>
          <?php } ?>
          <!-- End: Add Interest -->

          <!--Close database connection -->
          <?php mysqli_close($conn); ?>

        </div>
      </div>
    </div>
    <!-- End: Main Content -->


    <!-- Start: Footer -->
    <?php include ('common/footer.php'); ?>
    <!-- End: Footer -->

  </div>
  <!-- End: Main Panel -->
</div>
