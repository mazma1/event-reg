<?php
  $id = $_GET['id'];
?>

<div class="wrapper">

  <!-- Start: Side Bar -->
  <?php
  include ('common/sidebar.php');
  ?>
  <!-- End: Side Bar -->

  <!-- Start: Main Panel -->
  <div class="main-panel">

    <!-- Start: Nav Bar -->
      <?php include ('common/navbar.php') ?>
    <!-- End: Nav Bar -->

    <!-- Start: Main Content -->
    <div class="content">

      <div class="container-fluid">
        <div class="row">

          <!-- Start: User Details -->
          <div class="col-lg-6 col-md-6">
            <div class="card" style="border: 1px solid #d2d0cc;">
              <div class="header">
                  <h4 class="title">Personal Profile</h4>
              </div>
              <hr>


                <div class="content">

                  <?php

                  require("../../includes/db-connectvars.php");

                  $user_sql = " SELECT *
                                FROM users
                                WHERE id = '$id'";

                  $user_data = mysqli_query($conn, $user_sql);


                  $industry_sql = " SELECT *
                                    FROM users
                                    JOIN industry
                                    ON users.industry = industry.id
                                    WHERE users.id = '$id'";

                  $industry_data = mysqli_query($conn, $industry_sql);




                  while ($user_row = mysqli_fetch_array($user_data)) {

                  ?>

                  <div class="">
                    <p><strong>First Name:</strong></p>
                    <p><?php echo $user_row['first_name']; ?></p>
                  </div>

                  <div class="pt24">
                    <p><strong>Last Name:</strong></p>
                    <p><?php echo $user_row['last_name']; ?></p>
                  </div>

                  <div class="pt24">
                    <p><strong>Mobile Number:</strong></p>
                    <p><?php echo $user_row['mobile_number']; ?></p>
                  </div>

                  <div class="pt24">
                    <p><strong>Email:</strong></p>
                    <p><?php echo $user_row['email']; ?></p>
                  </div>

                  <div class="pt24">
                    <p><strong>Industry:</strong></p>
                    <p>
                      <?php
                        while ($industry_row = mysqli_fetch_array($industry_data)) {
                          echo $industry_row['industry'];
                        }?>
                    </p>
                  </div>

                  <?php } ?>
                </div>
            </div>
            <?php

            ?>
            <a class="btn btn-info btn-fill dashboard-btn" href="?page=users&p=1" role="button">Back</a>
          </div>
          <!-- End: User Details -->

          <!-- Start: User's Registered Events -->
          <div class="col-lg-6 col-md-6">
            <div class="card" style="border: 1px solid #d2d0cc; min-height: 528px;">

              <?php
                $s_n = 1;
                $regcount_sql = " SELECT *, count(events_id) as count
                                  FROM events
                                  LEFT JOIN registrants
                                  ON events.id = registrants.events_id
                                  WHERE registrants.users_id = '$id'
                                  GROUP BY events_id";
                $regcount_data = mysqli_query($conn, $regcount_sql);

                $events = mysqli_num_rows($regcount_data);

                if ($events > 1) { ?>
                  <div class="header">
                      <h4 class="title">Registered for <strong><?php echo mysqli_num_rows($regcount_data); ?></strong> Events</h4>
                  </div>
          <?php }
                elseif ( $events == 1 ) { ?>
                  <div class="header">
                      <h4 class="title">Registered for <strong><?php echo mysqli_num_rows($regcount_data); ?></strong> Event</h4>
                  </div>
          <?php }
                elseif ( $events == 0 ) {?>
                <div class="header">
                    <h4 class="title">User has no registered event</h4>
                </div>
                <?php } ?>
                <hr>
                <?php while ($regcount_row = mysqli_fetch_array($regcount_data)) { ?>
                <div class="content" style="padding-top: 0;">
                  <table class="table ">
                    <thead>
                      <tr>
                        <td width='1%'><?php echo $s_n; ?></td>
                        <td style="font-size: 16px;"><?php echo $regcount_row['event']; ?></td>
                      </tr>
                <?php $s_n++;
              } ?>
                    </thead>
                  </table>
                </div>

            </div>
          </div>
          <!-- End: User's Registered Events -->

        </div>
      </div>

    </div>
    <!-- End: Main Content -->

    <!-- Start: Footer -->
    <?php include ('common/footer.php'); ?>
    <!-- End: Footer -->

  </div>
</div>
