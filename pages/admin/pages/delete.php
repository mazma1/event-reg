<?php

  $user = $_GET['user'];
  $event = $_GET['event_id'];


  require("../../includes/db-connectvars.php");

  // User Details SQL
  $userdetail_sql = "SELECT * FROM registrants
                     WHERE users_id = '$user' AND events_id = '$event'";

  $userdetail_result = mysqli_query($conn, $userdetail_sql);

  if (isset($_POST['submit'])) {
    while ($userdetail_row = mysqli_fetch_array($userdetail_result)) {

      $delete_sql = "DELETE FROM registrants
                     WHERE users_id = '" . $userdetail_row['users_id'] . "' AND events_id = '" . $userdetail_row['events_id'] . "' ";

      $delete_result = mysqli_query($conn, $delete_sql);

      $_SESSION['delete'] = "Registrant successfully deleted!";

      $success_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/?page=registrant&event_id=' . $event;
      header('Location: ' . $success_url);
    }

  }
?>

<div class="wrapper">

  <!-- Start: Side Bar -->
  <?php
  include ('common/sidebar.php');
  ?>
  <!-- End: Side Bar -->

  <!-- Start: Main Panel -->
  <div class="main-panel">

    <!-- Start: Nav Bar -->
      <?php include ('common/navbar.php') ?>
    <!-- End: Nav Bar -->

    <!-- Start: Main Content -->
    <div class="content">

      <div class="container-fluid">
        <div class="row">

          <!-- Start: Delete Card-->
          <div class="col-md-6 col-md-offset-3">
            <div class="card" style="border: 1px solid #d2d0cc;">

              <div class="header">
                <h4 class="title" style="font-weight:500; padding-top: 12px;">DELETE</h4>
              </div>
              <hr>

              <div class="content" style="padding-top: 0;">
                <?php

                //Retrieve Full Name of user to be deleted
                $name_sql = " SELECT * FROM registrants
                              JOIN users
                              ON registrants.users_id = users.id
                              WHERE users_id = '$user' AND events_id = '$event'";

                $name_result = mysqli_query($conn, $name_sql);

                //Retrieve event user to be deleted registered for
                $event_sql = "SELECT * FROM registrants
                              JOIN events
                              ON registrants.events_id = events.id
                              WHERE users_id = '$user' AND events_id = '$event'";

                $event_result = mysqli_query($conn, $event_sql);

                while ($name_row = mysqli_fetch_array($name_result)) {
                ?>


                <!-- Delete prompt message -->
                <p>
                  Are you sure you want to delete <strong><?php echo $name_row['first_name'] . " " . $name_row['last_name']; }?></strong> from the list of registrants of the event, <strong><?php
                                                      while ($event_row = mysqli_fetch_array($event_result)) {
                                                        echo $event_row['event']; }?></strong>?
                </p>

                <div style="padding-top: 30px; padding-bottom: 15px;">
                  <form class="" action="" method="post">
                    <button type="submit" name="submit" class="btn btn-info btn-fill dashboard-btn">YES</button>
                    <a class="btn btn-danger btn-fill dashboard-btn" href="?page=registrant&event_id=<?php echo $event; ?>" role="button">CANCEL</a>
                  </form>
                </div>

              </div>

            </div>
          </div>
          <!-- End: Delete Card -->
        </div>
      </div>
    </div>
    <!-- End: Main Content -->

    <!-- Start: Footer -->
    <?php include ('common/footer.php'); ?>
    <!-- End: Footer -->
    
  </div>
</div>
