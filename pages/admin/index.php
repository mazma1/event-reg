<?php
  include("../../includes/config.php");
 ?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Admin | Event Registration</title>

    <!-- Start: Header Scripts and CSS Files -->
    <?php
      include("common/header-scripts.php");
     ?>
    <!-- End: Header Scripts and CSS Files -->

  </head>

  <body>

    <?php
    // Start the session
    session_start();
    ?>

    <?php

    if (isset ($_SESSION ['username'])) {
      if ($_GET['page'] == 'dashboard' || $_GET['page'] == '') {
        include ('pages/dashboard.php');
      }

      if ($_GET['page'] == 'logout') {
        include ('pages/logout.php');
      }

      if ($_GET['page'] == 'event') {
        include ('pages/event.php');
      }

      if ($_GET['page'] == 'registrant') {
        include ('pages/registrant.php');
      }

      if ($_GET['page'] == 'view') {
        include ('pages/view.php');
      }

      if ($_GET['page'] == 'edit') {
        include ('pages/edit.php');
      }

      if ($_GET['page'] == 'delete') {
        include ('pages/delete.php');
      }

      if ($_GET['page'] == 'industry') {
        include ('pages/industry.php');
      }

      if ($_GET['page'] == 'interest') {
        include ('pages/interest.php');
      }

      if ($_GET['page'] == 'dashboard-view') {
        include ('pages/dashboard-view.php');
      }

      if ($_GET['page'] == 'users') {
        include ('pages/users.php');
      }

      if ($_GET['page'] == 'userview') {
        include ('pages/userview.php');
      }
    }
    else {
        if ($_GET['page'] == 'login') {
          include ('pages/login.php');
        }
        else {
          $home_url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/?page=login';
          header('Location: ' . $home_url);
        }
    }

    ?>


    <!-- Start: Footer Scripts and JS Files -->
    <?php
      include("common/footer-scripts.php");
     ?>
    <!-- End: Footer Scripts and JS Files -->


  </body>



</html>
